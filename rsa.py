import secrets
import random
import sys
from datetime import datetime


def multiply(num1, num2):
    num1 = str(num1)
    num2 = str(num2)
    len1 = len(num1)
    len2 = len(num2)
    if len1 == 0 or len2 == 0:
        return "0"

    result = [0] * (len1 + len2)

    i_n1 = 0
    i_n2 = 0

    for i in range(len1 - 1, -1, -1):
        carry = 0
        n1 = ord(num1[i]) - 48

        i_n2 = 0

        for j in range(len2 - 1, -1, -1):

            n2 = ord(num2[j]) - 48

            summ = n1 * n2 + result[i_n1 + i_n2] + carry

            carry = summ // 10

            result[i_n1 + i_n2] = summ % 10

            i_n2 += 1

        if (carry > 0):
            result[i_n1 + i_n2] += carry

        i_n1 += 1

    i = len(result) - 1
    while (i >= 0 and result[i] == 0):
        i -= 1

    if (i == -1):
        return "0"

    s = ""
    while (i >= 0):
        s += chr(result[i] + 48)
        i -= 1

    return int(s)

# It returns (x^y) % p


def power(x, y, p):
    res = 1
    x = x % p
    while (y > 0):
        if ((y & 1) == 1):
            res = (res * x) % p
        y = y >> 1      # y = y/2
        x = (x * x) % p

    return res


def miillerTest(d, n):

    a = 2 + middleSq(1023)

    x = power(a, d, n)

    if (x == 1 or x == n - 1):
        return True

    while (d != n - 1):
        x = (x * x) % n
        d *= 2

        if (x == 1):
            return False
        if (x == n - 1):
            return True

    return False


def isPrime(n, k):
    if (n <= 1 or n == 4):
        return False
    if (n <= 3):
        return True

    d = n - 1
    while (d % 2 == 0):
        d //= 2

    for i in range(k):
        if (miillerTest(d, n) == False):
            return False
    return True


def makePrime():
    a = 0
    while(True):
        a += 1
        n = middleSq(1024)
        if isPrime(n, 10):
            print(str(a) + ' attempts')
            return n


def makeED(phi):
    a = 0
    while(True):
        a += 1
        e = middleSq(1023)
        gcde = gcdExtended(e, phi)
        if int(gcde[0]) == 1 and gcde[1] > 0:
            print(str(a) + ' attempts')
            return e, int(gcde[1])


list = [chr(i) for i in range(0,256)]

def textToDec(m):
    c = 0
    for i in range(1, len(m)+1):
        c += list.index(m[-1 * i]) * 256 ** (i-1)
    return c

# our message will be a 128 base string
def decToText(c):
    str1 = ''
    while (c != 0):
        a = c % 256
        str1 = list[a] + str1
        c = c//256
    return str1

def hashedList():
    hList = []
    try:
        with open('message.txt') as f:
            l = f.readlines()
            for i in l:
                hList += [i]
    except FileNotFoundError:
        print('message.txt is not found!')
    return hList


def makeKeys():
    publicKey = []
    secretKey = []

    try:
        with open('publicKey.txt') as f:
            l = f.readlines()
            if l != []:
                publicKey = [int(l[0]), int(l[1])]
            else:
                raise FileNotFoundError
        with open('secretKey.txt') as f:
            l = f.readlines()
            if l != []:
                secretKey = [int(l[0]), int(l[1])]
            else:
                raise FileNotFoundError
        print('Keys are available...')
    except FileNotFoundError:
        print('Making keys...')
        p = makePrime()
        #print('p: '+ str(p))
        q = makePrime()
        #print('q: '+ str(q))
        n = int(multiply(p, q))
        phi = int(multiply(p - 1, q - 1))
        e, d = makeED(phi)
        secretKey = [d, n]
        with open('secretKey.txt', 'w') as f:
            f.write('{}\n{}'.format(secretKey[0], secretKey[1]))
        publicKey = [e, n]
        with open('publicKey.txt', 'w') as f:
            f.write('{}\n{}'.format(publicKey[0], publicKey[1]))

        print('Successful')
    return secretKey, publicKey


def gcdExtended(a, b):

    # Base Case
    if a == 0:
        return b, 0, 1

    gcd, x1, y1 = gcdExtended(b % a, a)

    x = y1 - (b//a) * x1
    y = x1

    return gcd, x, y


def listMessage(m, n):
    listed = []
    p = m
    while p != '':
        td = textToDec(p)
        if(td >= n):
            p = p[:len(p)//2]
        else:
            listed += [td]
            m = m[len(p):]
            p = m
    return listed

# random number


def middleSq(size):
    seed = datetime.now().microsecond
    seed = str(seed)
    result = ''
    for i in range(size//19):
        seed = str(int(seed) ** 2)
        seed = seed.zfill(12)
        seed = str(int(seed[3:9]) ^ 111111)
        result += seed
    return int(result)

n = int(input('1- encrypt message to message.txt\n2- decrypt message from message.txt\n3- Make Keys\n=>'))
if (n == 1):
    secretKey, publicKey = makeKeys()
    m = input('Enter the message you want to encrypt: ')
    message = listMessage(m, publicKey[1])
    # codig the message
    with open('message.txt', 'w') as f:
        for i in message:
            c = power(i, publicKey[0], publicKey[1])
            f.write(str(c)+'\n')
elif (n == 2):
    secretKey, publicKey = makeKeys()
    message = hashedList()
    hashedMessage = []

    messageStr = ''
    for c in message:
        un = power(int(c), secretKey[0], secretKey[1])
        messageStr += decToText(un)
    
    print('the Message: ' + messageStr)
elif(n == 3):
    secretKey, publicKey = makeKeys()
